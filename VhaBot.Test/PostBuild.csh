#!/bin/tcsh -fv

# ========================
# === Check build mode ===
# ========================

if ( "$3" == "Release" ) then
    xmessage VhaBot.Test is not available in Release mode
    exit 
endif

# ==================================
# === Check if config.xml exists ===
# ==================================

if ( ! -e "$1/config.xml" ) then
    xmessage Please create config.xml as outlined in HowTo.txt
#    exit
endif

# ======================================
# === Start building our debug setup ===
# ======================================
cd $1
if ( ! -d "VhaBot.Test/bin/$3/plugins" ) then
    mkdir "VhaBot.Test/bin/$3/plugins"
else
    rm -f VhaBot.Test/bin/$3/plugins/*
endif

if ( ! -d VhaBot.Test/bin/$3/data ) then
    mkdir VhaBot.Test/bin/$3/data
endif
cp Extra/data/* VhaBot.Test/bin/$3/data

cp Plugins.Default/*.cs VhaBot.Test/bin/$3/Plugins/
cp Plugins.Default/*.dll VhaBot.Test/bin/$3/Plugins/
cp Plugins.Raid/*.cs VhaBot.Test/bin/$3/Plugins/
cp Plugins.Raid/*.dll VhaBot.Test/bin/$3/Plugins/
cp Plugins.Llie/*.cs VhaBot.Test/bin/$3/Plugins/
cp Plugins.Llie/*.dll VhaBot.Test/bin/$3/Plugins/

cp Extra/dimensions.xml VhaBot.Test/bin/$3/
cp Extra/text.mdb VhaBot.Test/bin/$3/

if ( "$3" == "Vhanet" ) then
    cp Plugins.Vhanet/*.cs VhaBot.Test/bin/$3/Plugins/
    cp Plugins.Vhanet/*.dll VhaBot.Test/bin/$3/Plugins/
endif

cp config.xml VhaBot.Test/bin/$3/

