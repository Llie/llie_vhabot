@echo off

rem ========================
rem === Check build mode ===
rem ========================

IF NOT %3==Release GOTO :OK1
wscript "%~2\MessageBox.js" VhaBot.Test is not available in Release mode
exit /b 1
:OK1

rem ==================================
rem === Check if config.xml exists ===
rem ==================================

IF EXIST "%~1\Config.xml" GOTO OK2
wscript "%~2\MessageBox.js" Please create Config.xml as outlined in HowTo.txt
exit /b 1
:OK2

rem ======================================
rem === Start building our debug setup ===
rem ======================================
cd /d %1
mkdir VhaBot.Test\bin\%3\Plugins
del /Q VhaBot.Test\bin\%3\Plugins\*

mkdir VhaBot.Test\bin\%3\data
copy /Y Extra\data\* VhaBot.Test\bin\%3\data

copy /Y Plugins.Default\*.cs VhaBot.Test\bin\%3\Plugins\
copy /Y Plugins.Default\*.dll VhaBot.Test\bin\%3\Plugins\
copy /Y Plugins.Raid\*.cs VhaBot.Test\bin\%3\Plugins\
copy /Y Plugins.Raid\*.dll VhaBot.Test\bin\%3\Plugins\
copy /Y Plugins.Llie\*.cs VhaBot.Test\bin\%3\Plugins\
copy /Y Plugins.Llie\*.dll VhaBot.Test\bin\%3\Plugins\

copy /Y Extra\dimensions.xml VhaBot.Test\bin\%3\
copy /Y Extra\text.mdb VhaBot.Test\bin\%3\

IF NOT %3==Vhanet GOTO :NO_VHANET
copy /Y Plugins.Vhanet\*.cs VhaBot.Test\bin\%3\Plugins\
copy /Y Plugins.Vhanet\*.dll VhaBot.Test\bin\%3\Plugins\
:NO_VHANET

copy /Y config.xml VhaBot.Test\bin\%3\
