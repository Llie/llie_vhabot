using System;

namespace VhaBot
{
public class IrcMessageArgs
{
	private string _message;

	public string Message
	{
		get
		{
			return this._message;
		}
	}

	public IrcMessageArgs(BotShell bot, string message)
	{
		this._message = message;
	}
}
}