using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using VhaBot.Communication;
using AoLib.Net;

namespace VhaBot
{

    public abstract class PluginBase : MarshalByRefObject
    {
        private bool _locked;
        private string _name;
        private string _internalName;
        private int _version;
        private string _author;
        private string _description;
        private PluginState _defaultState;
        private string[] _dependencies;
        private Command[] _commands;

        public string Name {
            set { if (this._locked) { throw new Exception(); } this._name = value; }
            get { return this._name; }
        }
        public string InternalName {
            set { if (this._locked) { throw new Exception(); } this._internalName = value.ToLower(); }
            get { return this._internalName; }
        }
        public int Version {
            set { if (this._locked) { throw new Exception(); } this._version = value; }
            get { return this._version; }
        }
        public string Author {
            set { if (this._locked) { throw new Exception(); } this._author = value; }
            get { return this._author; }
        }
        public string Description {
            set { if (this._locked) { throw new Exception(); } this._description = value; }
            get { return this._description; }
        }
        public PluginState DefaultState {
            set { if (this._locked) { throw new Exception(); } this._defaultState = value; }
            get { return this._defaultState; }
        }
        public string[] Dependencies
        {
            set { if (this._locked) { throw new Exception(); } this._dependencies = value; }
            get { if (this._dependencies != null) { return this._dependencies; } return new string[0]; }
        }
        public Command[] Commands
        {
            set { if (this._locked) { throw new Exception(); } this._commands = value; }
            get { if (this._commands != null) { return this._commands; } return new Command[0]; }
        }

        internal void Init() { this._locked = true; }

        public virtual void OnLoad(BotShell bot) { }
        public virtual void OnUnload(BotShell bot) { }
        public virtual void OnInstall(BotShell bot) { }
        public virtual void OnUninstall(BotShell bot) { }
        public virtual void OnUpgrade(BotShell bot, Int32 version) { }

        public virtual void OnCommand(BotShell bot, CommandArgs e) { }
        public virtual void OnUnauthorizedCommand(BotShell bot, CommandArgs e) { }

        public virtual string OnHelp(BotShell bot, string command) { return null; }
        public virtual string OnCustomConfiguration(BotShell bot, string key) { return null; }

        public virtual void OnPluginMessage(BotShell bot, PluginMessage message) { }
        public virtual void OnBotMessage(BotShell bot, BotMessage message) { }

        public override string ToString() { return this.Name + " v" + this.Version; }

        public void OnFirehandleCommand(object botArgs)
        {
            BotArgs Args = botArgs as BotArgs;
            var botShell = Args.Bot;
            var commandArgs = Args.Args;
            OnCommand(botShell, commandArgs);
        }

        public void FireOnCommand(BotShell bot, CommandArgs args)
        {
            try
            {
                BotArgs botArgs = null;
                if (args.Authorized)
                {
                    try
                    {
                        botArgs = new BotArgs {Bot = bot, Args = args};
                        var thread = new Thread(OnFirehandleCommand);
                        thread.Start(botArgs);
                    }
                    catch
                    {
                        this.OnCommand(bot, args);
                    }
                }
                else
                    this.OnUnauthorizedCommand(bot, args);
            }
            catch (Exception ex)
            {
                CommandArgs e = (CommandArgs)args;
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Error Report");

                window.AppendHighlight("Error: ");
                window.AppendNormal(ex.Message);
                window.AppendLinkEnd();
                window.AppendLineBreak();

                window.AppendHighlight("Source: ");
                window.AppendNormal(ex.Source);
                window.AppendLinkEnd();
                window.AppendLineBreak();

                window.AppendHighlight("Target Site: ");
                window.AppendNormal(ex.TargetSite.ToString());
                window.AppendLinkEnd();
                window.AppendLineBreak();

                window.AppendHighlight("Stack Trace:");
                window.AppendLineBreak();
                window.AppendNormal(ex.StackTrace);
                window.AppendLinkEnd();
                window.AppendLineBreak();

                bot.SendReply(e, "There has been an error while executing this command »» " + window.ToString("More Information"));
                BotShell.Output("[Plugin Execution Error] " + ex.ToString());
            }
        }
    }
}
