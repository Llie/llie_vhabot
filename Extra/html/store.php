<?php
// this will display php info -- uncomment this and see that you have SQLite3
// phpinfo();

// Global variables that you may want to  change.

// By default ll_Store.cs creates lliestore.db3 in the bot's config
// directory.  You should supply the full path to that file here:
$dbfile="lliestore.db3";

$max_results=100;

// Icons from auno
$imgsrc="http://auno.org/res/aoicons/";

// Item links to auno
// $dburl="http://auno.org/ao/db.php";
// Item links to xyphos
$dburl="http://www.xyphos.com/ao/aodb.php";
?>

<BODY>

<!-- uncomment the following section of HTML if you have your own CSS
     stylesheet you would like to use -->
<!--
<link rel="stylesheet" type="text/css" media="screen" href="store.css" />
-->

<!-- The following stylesheet is for demonstration purposes.  Delete 
     this section if you have your own stylesheet -->
<!-- CUT HERE -->
<style type="text/css">
body {
    background:#FFFFFF;
    font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;
	font-size:12px;
    color:#000000;
}

div.SearchFrame{
    float: left;
    border: 1px solid #808080;
    padding: 20px;
    margin: 10px;
    width: 150px;
}

div.ResultsFrame{
    float: left;
    border: 1px solid #808080;
    padding: 20px;
    margin: 10px;
    width: 600px;
}

table {
    border-collapse: collapse;
    padding: 0px;
    margin: 0px;
}

tr {
    border-bottom: 1px solid #808080;
    border-top: 1px solid #808080;
}

tr.OddRow {
    background:#E0E0E0;
}

td {
    padding: 10px;
}

</style>
<!-- CUT HERE -->

<?php

// From here down you should only need to change things to get the
// HTML to format in a more desirable way

// the following function displays query results
function DisplayResults( $query )
{

    global $imgsrc;
    global $dburl;
    global $max_results;

    $counter = 0;

    echo "<table>";
    echo "<tr>";
    echo "<td></td>";
    echo "<td>Qty</td>";
    echo "<td>QL</td>";
    echo "<td>Name</td>";
    echo "<td>Price</td>";
    echo "<td>Seller</td>";
    echo "</tr>\n";

    while ( ($row = $query->fetchArray()) && ( $counter < $max_results ) )
    {
        echo "<tr";
        if ( $counter % 2 == 0 ) echo " class=\"EvenRow\"";
        else echo " class=\"OddRow\"";
        echo " >";

        // icon
        echo "<td><img src=\"" . $imgsrc . $row["iconid"] . ".gif\"></td>";

        // qty if > 0
        echo "<td>";
        if ( $row['quantity'] > 0 ) {
        	echo $row['quantity'];
        }
        echo "</td>";

        // QL
        echo "<td>" . $row['ql'] . "</td>";

        // link
        echo "<td><a href=\"" . $dburl .  "?id=" .
            $row['lowid'] . "&ql=" . $row['ql'] . "\" target=_blank>" .
            $row['name'] . "</a></td>";

        // price
        echo "<td>";
        if ( $row['price'] > 0 ) {
            $iprice = intval ( $row['price'] );
            $exp = log10( $row['price'] );
            $mod = "";
            if ( $exp >= 9 )
            {
                $iprice /= 1000000000; 
                $mod = "B";
            }
            else if ( $exp >= 6 )
            {
                $iprice /= 1000000; 
                $mod = "M";
            }
            else if ( $exp >= 3 )
            {
                $iprice /= 1000;
                $mod = "K";
            }
            echo $iprice . $mod;
        }
        else
        {
            echo "accepting offers";
        }       
        echo "</td>";

        // seller / buy / etc...
        echo "<td><a href=\"store.php?show=" . $row['seller'] . "\">"
            . $row['seller'] . "</a></td>";

        echo "</tr>\n";
        $counter ++;
    }
    echo "</table>";
    
}

// implements SQLite3 database
class MyDB extends SQLite3
{
    function __construct()
    {
        global $dbfile;
        $this->open($dbfile);
    }
}

?> 

<DIV class="TopLevelContainer">

<DIV class="SearchFrame">

Search:<br>
<form action=store.php method=get>
<input type=text name=search><br>
<input type=submit>
</form>

Search for exact QL:<br>
<form action=store.php method=get>
QL: <input type=text size=3 name=loql><br>
<input type=text name=search><br>
<input type=submit>
</form>

Search for QL range:<br>
<form action=store.php method=get>
QL: <input type=text size=3 name=loql> - 
<input type=text size=3 name=hiql><br>
<input type=text name=search><br>
<input type=submit>
</form>

Show user's store:
<form action=store.php method=get>
<input type=text name=show><br>
<input type=submit>
</form>

</DIV>

<DIV class="ResultsFrame">

<?php 
// create a new database object
$db = new MyDB();

$tables = $db->query( "SELECT name FROM sqlite_master WHERE type ='table'" );
$row = $tables->fetchArray();
$table_name = $row['name'];

// put this in one frame
if ( array_key_exists ( 'search', $_GET ) )
{

    $search_string = str_replace( "+", "%", "%" . $_GET['search'] . "%" );
    $search_string = str_replace( " ", "%", $search_string );
    $search_string = str_replace( "%20", "%", $search_string );

    if ( array_key_exists ( 'loql', $_GET ) &&
         array_key_exists ( 'hiql', $_GET ) )
        $sqlcommand =  "SELECT * FROM " . $table_name . 
            " WHERE name LIKE '" . $search_string .
            "' AND ql BETWEEN " . $_GET['loql'] . " AND " . $_GET['hiql'] .
            " ORDER BY lowid, ql";
    else if ( array_key_exists ( 'loql', $_GET ) )
        $sqlcommand = "SELECT * FROM " . $table_name .
            " WHERE name LIKE '" . $search_string .
            "'  AND ql = " . $_GET['loql'] .
            " ORDER BY lowid, ql";
    else
        $sqlcommand =  "SELECT * FROM " . $table_name .
            " WHERE name LIKE '" . $search_string .
            "' ORDER BY lowid, ql";

    $query = $db->query( $sqlcommand );
    if (!$query)
        die ( $db->lastErrorMsg() );

    DisplayResults ( $query );

}
else if ( array_key_exists ( 'show', $_GET ) )
{ 
    $query = $db->query( "SELECT * FROM " . $table_name .
                         " WHERE lower(seller) = lower('" . $_GET['show'] .
                         "') ORDER BY lowid, ql" );
    if (!$query)
        die ( $db->lastErrorMsg() );

    DisplayResults ( $query );
    
}
?> 

</DIV>

</DIV>

<?php 
?>

</BODY>
