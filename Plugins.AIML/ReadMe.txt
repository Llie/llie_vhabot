== VhaBot AIML Plug-in ==

This VhaBot plug-in allows VhaBot to respond to user input with
scripted simulated intelligence.

The supplied AIML files includes rudimentary Anarchy Online knowledge
in the ao.aiml file.  If you add more AO specific knowledge to this
file, please share your additions with me.

Installation:

Copy aiml_Chatbot.cs and AIMLbot.dll to your bot's plugins directory.

Copy the aiml directory to your bot directory.

Edit the file aiml/Settings.xml with attributes for your bot.

Start up your bot, as usual, and use the !plugins command to activate
this plug-in.

Notes:

This plug-in uses AIMLbot from http://aimlbot.sourceforge.net/

The included aiml set is from the free ALICE AIML set which can be
downloaded from http://code.google.com/p/aiml-en-us-foundation-alice/

