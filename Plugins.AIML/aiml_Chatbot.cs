using System;
using System.Collections.Generic;
using System.IO;
using AIMLbot;
using AoLib.Utils;

namespace VhaBot.Plugins
{

    public class AIMLChatBot : PluginBase
    {

        AIMLbot.Bot myBot;

        // maximum number of simultaneous conversations ( zero is unlimited )
        private Int32 _max_users = 10;
        Dictionary<string,AIMLbot.User> _users;

        public AIMLChatBot()
        {
            this.Name = "AIML Chat Bot";
            this.InternalName = "aimlChatBot";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 100;
            this.Description = "Chatbot.";
            this.Commands = new Command[] {
                new Command( "ai", true, UserLevel.Member)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            myBot = new AIMLbot.Bot();
            if ( File.Exists ( "AIMLSettings.xml" ) )
                myBot.loadSettings("AIMLSettings.xml");
            else
                myBot.loadSettings("aiml/Settings.xml");
            myBot.isAcceptingUserInput = false;
            myBot.loadAIMLFromFiles();
            myBot.isAcceptingUserInput = true;
            _users = new Dictionary<string,AIMLbot.User>();

            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(ConfigurationChangedEvent);
            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "maxusers", "Maximum number of simultaneous users", this._max_users, 0, 5, 10, 20, 50);

            bot.Events.UserLogoffEvent += new UserLogoffHandler(Events_UserLogoffEvent);
            bot.Events.UserLeaveChannelEvent += new UserLeaveChannelHandler(Events_UserLeaveChannelEvent);

        }

        public override void OnUnload(BotShell bot)
        {
            bot.Events.UserLogoffEvent -= new UserLogoffHandler(Events_UserLogoffEvent);
            bot.Events.UserLeaveChannelEvent -= new UserLeaveChannelHandler(Events_UserLeaveChannelEvent);
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            LoadConfiguration( bot );
        }

        private void LoadConfiguration(BotShell bot)
        {
            this._max_users = bot.Configuration.GetInteger(this.InternalName, "maxusers", this._max_users);
        }

        private void Events_UserLogoffEvent(BotShell bot, UserLogoffArgs e)
        {
            if ( _users.ContainsKey( e.Sender ) )
                _users.Remove(e.Sender);
        }

        private void Events_UserLeaveChannelEvent(BotShell bot, UserLeaveChannelArgs e)
        {
            if ( _users.ContainsKey( e.Sender ) )
                _users.Remove(e.Sender);
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            AIMLbot.User curUser = null;

            switch (e.Command)
            {

            case "ai":

                if ( _users.ContainsKey( e.Sender ) )
                    curUser = _users[e.Sender];
                else if ( ( _max_users > 0 ) && ( _users.Count < _max_users ) )
                {
                    curUser = new AIMLbot.User( e.Sender, myBot );
                    _users[e.Sender] = curUser;
                }
                else
                {
                    bot.SendReply( e, "I'm sorry, " + e.Sender +
                                   ", I can't carry on more than " +
                                   _max_users.ToString() +
                                   " conversations at the same time.  " +
                                   "I'm only human, after all." );
                    return;
                }

                if ( curUser == null )
                {
                    bot.SendReply(e, "I'm sorry, " + e.Sender +
                                  ", I'm not feeling very social right now." );
                    return;
                }

                Request r = new Request( e.Words[0], curUser, myBot);
                Result res = myBot.Chat( r );
                bot.SendReply( e, res.Output );

                break;

            }

        }

        public override string OnHelp(BotShell bot, string command)
        {

            switch (command)
            {

            case "ai":
                return "Have a nice conversation with the bot.\n" +
                    "Usage: /tell " + bot.Character + " ai [chat]";

            }

            return null;

        }

    }

}
