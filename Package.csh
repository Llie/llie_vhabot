#!/bin/tcsh -f

cd $1

echo Creating directories
rm -rf Package
mkdir Package
mkdir Package/plugins
mkdir Package/data

echo Copying Assemblies
cp AoLib/bin/Release/AoLib.dll Package/
cp VhaBot.API/bin/Release/VhaBot.API.dll Package/
cp VhaBot.API/bin/Release/Mono.Data.SqliteClient.dll Package/
cp VhaBot.API/bin/Release/sqlite3.dll Package/
cp VhaBot.Communication/bin/Release/VhaBot.Communication.dll Package/
cp VhaBot.Configuration/bin/Release/VhaBot.Configuration.dll Package/
cp VhaBot.ConfigurationTool/bin/Release/Configure.exe Package/
cp VhaBot.Core/bin/Release/VhaBot.Core.dll Package/
cp VhaBot.CorePlugins/bin/Release/VhaBot.CorePlugins.dll Package/
cp VhaBot/bin/Release/VhaBot.exe Package/
cp VhaBot.Shell/bin/Release/VhaBot.Shell.exe Package/
cp VhaBot.Shell/bin/Release/VhaBot.Shell.exe.config Package/
cp VhaBot.Lite/bin/Release/VhaBot.Lite.exe Package/
cp VhaBot.Lite/bin/Release/VhaBot.Lite.exe.config Package/
cp Plugins.Default/*.cs Package/plugins/
cp Plugins.Default/*.dll Package/plugins/
cp Plugins.Llie/*.cs Package/plugins/
cp Plugins.Llie/*.dll Package/plugins/
cp Plugins.Raid/*.cs Package/plugins/
cp Plugins.Raid/*.dll Package/plugins/
cp -r Extra/* Package/
cp -r Extra/data/* Package/data/

