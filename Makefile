all:
	@ xbuild VhaBot2008.sln

clean:
	@ xbuild VhaBot2008.sln /t:Clean

test:
	@ xbuild VhaBot.Test/VhaBot.Test.csproj /p:SolutionDir=`pwd` /p:Configuration=Debug /p:OutputPath=VhaBot.Test/bin/Debug

release:
	@ xbuild VhaBot2008.sln /p:Configuration=Release

advanced:
	@ xbuild VhaBot2008.sln /p:Configuration=Advanced

all-clean:	clean
	@ rm -rf `find . \( -name "obj" -o -name "bin" \)`
	@ rm -rf `find . -name "Release"`
	@ rm -rf `find . -depth 2 -name "VhaBot.Test"`
	@ rm -rf Package

module:
	@ xbuild *.csproj /p:Configuration=Debug /p:OutputPath=bin/Debug
