using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using AoLib.Utils;
using AoLib.Net;

namespace VhaBot.CorePlugins
{
    public class ConfigurationManager : PluginBase
    {
        private Dictionary<string, string> Colors;
        public ConfigurationManager()
        {
            this.Name = "Configuration Manager";
            this.InternalName = "VhConfigurationManager";
            this.Author = "Vhab";
            this.Description = "Provides a UI for viewing and changing various dynamic settings";
            this.DefaultState = PluginState.Core;
            this.Version = 100;
            this.Commands = new Command[] {
                new Command("configuration", true, UserLevel.SuperAdmin),
                new Command("configuration reset", true, UserLevel.SuperAdmin),
                new Command("configuration set", true, UserLevel.SuperAdmin),
                new Command("configuration color", false, UserLevel.SuperAdmin),
                new Command("config", "configuration")
            };
        }

        public override void OnLoad(BotShell bot)
        {
            Colors = new Dictionary<string, string>
                {
                    {"Red Colors", ""},
                    {"IndianRed", "CD5C5C"},
                    {"LightCoral", "F08080"},
                    {"Salmon", "FA8072"},
                    {"DarkSalmon", "E9967A"},
                    {"LightSalmon", "FFA07A"},
                    {"Red", "FF0000"},
                    {"Crimson", "DC143C"},
                    {"FireBrick", "B22222"},
                    {"DarkRed", "8B0000"},
                    {"Pink Colors", ""},
                    {"Pink", "FFC0CB"},
                    {"LightPink", "FFB6C1"},
                    {"PaleVioletRed", "DB7093"},
                    {"HotPink", "FF69B4"},
                    {"DeepPink", "FF1493"},
                    {"MediumVioletRed", "C71585"},
                    {"Orange Colors", ""},
                    {"Orange", "FFA500"},
                    {"DarkOrange", "FF8C00"},
                    {"Coral", "FF7F50"},
                    {"Tomato", "FF6347"},
                    {"OrangeRed", "FF4500"},
                    {"Yellow Colors", ""},
                    {"LightYellow", "FFFFE0"},
                    {"LemonChiffon", "FFFACD"},
                    {"LightGoldenrodYellow", "FAFAD2"},
                    {"PapayaWhip", "FFEFD5"},
                    {"Moccasin", "FFE4B5"},
                    {"PeachPuff", "FFDAB9"},
                    {"PaleGoldenrod", "EEE8AA"},
                    {"Khaki", "F0E68C"},
                    {"Yellow", "FFFF00"},
                    {"Gold", "FFD700"},
                    {"DarkKhaki", "BDB76B"},
                    {"Green Colors", ""},
                    {"GreenYellow", "ADFF2F"},
                    {"Chartreuse", "7FFF00"},
                    {"LawnGreen", "7CFC00"},
                    {"Lime", "00FF00"},
                    {"PaleGreen", "98FB98"},
                    {"LightGreen", "90EE90"},
                    {"MediumSpringGreen", "00FA9A"},
                    {"SpringGreen", "00FF7F"},
                    {"YellowGreen", "9ACD32"},
                    {"LimeGreen", "32CD32"},
                    {"MediumSeaGreen", "3CB371"},
                    {"SeaGreen", "2E8B57"},
                    {"ForestGreen", "228B22"},
                    {"Green", "008000"},
                    {"OliveDrab", "6B8E23"},
                    {"Olive", "808000"},
                    {"DarkOliveGreen", "556B2F"},
                    {"DarkGreen", "006400"},
                    {"MediumAquamarine", "66CDAA"},
                    {"DarkSeaGreen", "8FBC8F"},
                    {"LightSeaGreen", "20B2AA"},
                    {"DarkCyan", "008B8B"},
                    {"Teal", "008080"},
                    {"Blue Colors", ""},
                    {"Cyan", "00FFFF"},
                    {"LightCyan", "E0FFFF"},
                    {"PaleTurquoise", "AFEEEE"},
                    {"Aqua", "00FFFF"},
                    {"Aquamarine", "7FFFD4"},
                    {"Turquoise", "40E0D0"},
                    {"MediumTurquoise", "48D1CC"},
                    {"DarkTurquoise", "00CED1"},
                    {"PowderBlue", "B0E0E6"},
                    {"LightSteelBlue", "B0C4DE"},
                    {"LightBlue", "ADD8E6"},
                    {"SkyBlue", "87CEEB"},
                    {"LightSkyBlue", "87CEFA"},
                    {"DeepSkyBlue", "00BFFF"},
                    {"CornflowerBlue", "6495ED"},
                    {"SteelBlue", "4682B4"},
                    {"CadetBlue", "5F9EA0"},
                    {"MediumSlateBlue", "7B68EE"},
                    {"DodgerBlue", "1E90FF"},
                    {"RoyalBlue", "4169E1"},
                    {"Blue", "0000FF"},
                    {"MediumBlue", "0000CD"},
                    {"DarkBlue", "00008B"},
                    {"Navy", "000080"},
                    {"MidnightBlue", "191970"},
                    {"Purple Colors", ""},
                    {"Lavender", "E6E6FA"},
                    {"Thistle", "D8BFD8"},
                    {"Plum", "DDA0DD"},
                    {"Violet", "EE82EE"},
                    {"Fuchsia", "FF00FF"},
                    {"Magenta", "FF00FF"},
                    {"Orchid", "DA70D6"},
                    {"MediumOrchid", "BA55D3"},
                    {"MediumPurple", "9370DB"},
                    {"SlateBlue", "6A5ACD"},
                    {"BlueViolet", "8A2BE2"},
                    {"DarkViolet", "9400D3"},
                    {"DarkOrchid", "9932CC"},
                    {"DarkMagenta", "8B008B"},
                    {"Purple", "800080"},
                    {"DarkSlateBlue", "483D8B"},
                    {"Indigo", "4B0082"},
                    {"Brown Colors", ""},
                    {"Cornsilk", "FFF8DC"},
                    {"BlanchedAlmond", "FFEBCD"},
                    {"Bisque", "FFE4C4"},
                    {"NavajoWhite", "FFDEAD"},
                    {"Wheat", "F5DEB3"},
                    {"BurlyWood", "DEB887"},
                    {"Tan", "D2B48C"},
                    {"RosyBrown", "BC8F8F"},
                    {"SandyBrown", "F4A460"},
                    {"Goldenrod", "DAA520"},
                    {"DarkGoldenrod", "B8860B"},
                    {"Peru", "CD853F"},
                    {"Chocolate", "D2691E"},
                    {"SaddleBrown", "8B4513"},
                    {"Sienna", "A0522D"},
                    {"Brown", "A52A2A"},
                    {"Maroon", "800000"},
                    {"White Colors", ""},
                    {"White", "FFFFFF"},
                    {"Snow", "FFFAFA"},
                    {"Honeydew", "F0FFF0"},
                    {"MintCream", "F5FFFA"},
                    {"Azure", "F0FFFF"},
                    {"AliceBlue", "F0F8FF"},
                    {"GhostWhite", "F8F8FF"},
                    {"WhiteSmoke", "F5F5F5"},
                    {"Seashell", "FFF5EE"},
                    {"Beige", "F5F5DC"},
                    {"OldLace", "FDF5E6"},
                    {"FloralWhite", "FFFAF0"},
                    {"Ivory", "FFFFF0"},
                    {"AntiqueWhite", "FAEBD7"},
                    {"Linen", "FAF0E6"},
                    {"LavenderBlush", "FFF0F5"},
                    {"MistyRose", "FFE4E1"},
                    {"Grey Colors", ""},
                    {"Gainsboro", "DCDCDC"},
                    {"LightGrey", "D3D3D3"},
                    {"Silver", "C0C0C0"},
                    {"DarkGray", "A9A9A9"},
                    {"Gray", "808080"},
                    {"DimGray", "696969"},
                    {"LightSlateGray", "778899"},
                    {"SlateGray", "708090"},
                    {"DarkSlateGray", "2F4F4F"},
                    {"Black", "000000"}
                };

        }

        public override void OnUnload(BotShell bot) { }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {
                case "configuration":
                    if (e.Args.Length == 0)
                        this.OnConfigurationCommand(bot, e);
                    else
                        this.OnConfigurationDisplayCommand(bot, e);
                    break;
                case "configuration set":
                    this.OnConfigurationSetCommand(bot, e);
                    break;
                case "configuration color":
                    this.OnConfigurationColorCommand(bot, e);
                    break;
                case "configuration reset":
                    this.OnConfigurationResetCommand(bot, e);
                    break;
            }
        }

        private void OnConfigurationCommand(BotShell bot, CommandArgs e)
        {
            string[] plugins = bot.Configuration.ListRegisteredPlugins();
            if (plugins == null || plugins.Length == 0)
            {
                bot.SendReply(e, "No configuration entries registered");
                return;
            }
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Configuration");
            foreach (string plugin in plugins)
            {
                PluginLoader loader = bot.Plugins.GetLoader(plugin);
                window.AppendNormalStart();
                window.AppendString("[");
                window.AppendBotCommand("Configure", "configuration " + plugin.ToLower());
                window.AppendString("] ");
                window.AppendColorEnd();

                window.AppendHighlight(loader.Name);
                window.AppendNormal(" (" + bot.Configuration.List(plugin).Length + " settings)");
                window.AppendLineBreak();
            }
            bot.SendReply(e, "Configuration »» ", window);
        }

        private void OnConfigurationDisplayCommand(BotShell bot, CommandArgs e)
        {
            string internalName = e.Args[0].ToLower();
            if (!bot.Plugins.Exists(internalName))
            {
                bot.SendReply(e, "No such plugin!");
                return;
            }
            ConfigurationEntry[] entires = bot.Configuration.List(internalName);
            if (entires.Length < 1)
            {
                bot.SendReply(e, "This plugin has no settings to configure");
                return;
            }
            RichTextWindow window = new RichTextWindow(bot);
            PluginLoader loader = bot.Plugins.GetLoader(internalName);
            window.AppendTitle("Configuration");
            foreach (ConfigurationEntry entry in entires)
            {
                window.AppendHighlight(entry.Name + ": ");
                window.AppendNormalStart();
                string command = "configuration set " + internalName + " " + entry.Key.ToLower();
                switch (entry.Type)
                {
                    case ConfigType.String:
                        string value1 = bot.Configuration.GetString(entry.Section, entry.Key, (string)entry.DefaultValue);
                        if (entry.Values != null && entry.Values.Length > 0)
                            window.AppendMultiBox(command, value1, entry.StringValues);
                        else
                        {
                            window.AppendString(value1 + " [");
                            window.AppendCommand("Edit", "/text /tell " + bot.Character + " " + command + " [text]");
                            window.AppendString("]");
                        }
                        break;
                    case ConfigType.Integer:
                        string value2 = bot.Configuration.GetInteger(entry.Section, entry.Key, (int)entry.DefaultValue).ToString();
                        if (entry.Values != null && entry.Values.Length > 0)
                            window.AppendMultiBox(command, value2, entry.StringValues);
                        else
                        {
                            window.AppendString(value2 + " [");
                            window.AppendCommand("Edit", "/text /tell " + bot.Character + " " + command + " [number]");
                            window.AppendString("]");
                        }
                        break;
                    case ConfigType.Boolean:
                        string value3 = "Off";
                        if (bot.Configuration.GetBoolean(entry.Section, entry.Key, (bool)entry.DefaultValue))
                            value3 = "On";
                        window.AppendMultiBox(command, value3, "On", "Off");
                        break;
                    case ConfigType.Username:
                        string value4 = bot.Configuration.GetUsername(entry.Section, entry.Key, (string)entry.DefaultValue);
                        window.AppendString(value4 + " [");
                        window.AppendCommand("Edit", "/text /tell " + bot.Character + " " + command + " [username]");
                        window.AppendString("]");
                        break;
                    case ConfigType.Date:
                        DateTime value5 = bot.Configuration.GetDate(entry.Section, entry.Key, (DateTime)entry.DefaultValue);
                        window.AppendString(value5.ToString("dd/MM/yyyy") + " [");
                        window.AppendCommand("Edit", "/text /tell " + bot.Character + " " + command + " [dd]/[mm]/[yyyy]");
                        window.AppendString("]");
                        break;
                    case ConfigType.Time:
                        TimeSpan value6 = bot.Configuration.GetTime(entry.Section, entry.Key, (TimeSpan)entry.DefaultValue);
                        window.AppendString(string.Format("{0:00}:{1:00}:{2:00}", Math.Floor(value6.TotalHours), value6.Minutes, value6.Seconds) + " [");
                        window.AppendCommand("Edit", "/text /tell " + bot.Character + " " + command + " [hh]:[mm]:[ss]");
                        window.AppendString("]");
                        break;
                    case ConfigType.Dimension:
                        string value7 = bot.Configuration.GetDimension(entry.Section, entry.Key, (Server)entry.DefaultValue).ToString();
                        Dimensions Servers = Dimensions.ParseXML( string.Empty );
                        string[] ServerNames = Servers.Names();
                        window.AppendMultiBox(command, value7, ServerNames );
                        break;
                    case ConfigType.Color:
                        string value8 = bot.Configuration.GetColor(entry.Section, entry.Key, (string)entry.DefaultValue);
                        window.AppendColorString(value8, value8);
                        window.AppendString(" [");
                        window.AppendBotCommand("Edit", "configuration color " + internalName + " " + entry.Key.ToLower());
                        window.AppendString("]");
                        break;
                    case ConfigType.Password:
                        string value9 = bot.Configuration.GetPassword(entry.Section, entry.Key, (string)entry.DefaultValue);
                        for (int i = 0; i < value9.Length; i++)
                            window.AppendString("*");
                        window.AppendString(" [");
                        window.AppendCommand("Edit", "/text /tell " + bot.Character + " " + command + " [password]");
                        window.AppendString("]");
                        break;
                    case ConfigType.Custom:
                        string value10 = bot.Configuration.GetCustom(entry.Section, entry.Key);
                        if (value10 != null)
                            window.AppendRawString(value10);
                        break;
                }
                window.AppendString(" [");
                window.AppendBotCommand("Default", "configuration reset " + internalName + " " + entry.Key.ToLower());
                window.AppendString("]");
                window.AppendColorEnd();
                window.AppendLineBreak();
            }
            bot.SendReply(e, "Configuration »» " + loader.Name + " »» ", window);
        }

        private void OnConfigurationSetCommand(BotShell bot, CommandArgs e)
        {
            if (e.Args.Length < 3)
            {
                bot.SendReply(e, "Usage: configuration set [plugin] [key] [value]");
                return;
            }
            if (!bot.Configuration.IsRegistered(e.Args[0], e.Args[1]))
            {
                bot.SendReply(e, "No such configuration entry");
                return;
            }
            string section = e.Args[0].ToLower();
            string key = e.Args[1].ToLower();
            string value = e.Words[2];
            ConfigType type = bot.Configuration.GetType(section, key);
            bot.Configuration.Exists(section, key);
            object objValue = null;
            bool error = false;
            switch (type)
            {
                case ConfigType.Boolean:
                    if (value.ToLower() == "on")
                    {
                        objValue = true;
                        break;
                    }
                    if (value.ToLower() == "off")
                    {
                        objValue = false;
                        break;
                    }
                    error = true;
                    break;
                case ConfigType.Color:
                    if (!Regex.Match(value, "^[#]?[0-9a-f]{6}$", RegexOptions.IgnoreCase).Success)
                        error = true;
                    objValue = value;
                    break;
                case ConfigType.Date:
                    Match dateMatch = Regex.Match(value, "^([0-2][0-9]|3[0-1])/([0]?[0-9]|1[0-2])/([0-9]{4})$", RegexOptions.IgnoreCase);
                    if (!dateMatch.Success)
                    {
                        error = true;
                        break;
                    }
                    try
                    {
                        int day = Convert.ToInt32(dateMatch.Groups[1].Value);
                        int month = Convert.ToInt32(dateMatch.Groups[2].Value);
                        int year = Convert.ToInt32(dateMatch.Groups[3].Value);
                        objValue = new DateTime(year, month, day, 0, 0, 0, DateTimeKind.Utc);
                    }
                    catch { error = true; }
                    break;
                case ConfigType.Dimension:
                    try { objValue = (Server)Enum.Parse(typeof(Server), Format.UppercaseFirst(value)); }
                    catch { error = true; }
                    break;
                case ConfigType.Integer:
                    try { objValue = Convert.ToInt32(value); }
                    catch { error = true; }
                    break;
                case ConfigType.String:
                case ConfigType.Password:
                    objValue = value;
                    break;
                case ConfigType.Time:
                    Match timeMatch = Regex.Match(value, "^([0-9]+):([0-5][0-9]|60):([0-5][0-9]|60)$", RegexOptions.IgnoreCase);
                    if (!timeMatch.Success)
                    {
                        error = true;
                        break;
                    }
                    try
                    {
                        int hours = Convert.ToInt32(timeMatch.Groups[1].Value);
                        int minutes = Convert.ToInt32(timeMatch.Groups[2].Value);
                        int seconds = Convert.ToInt32(timeMatch.Groups[3].Value);
                        objValue = new TimeSpan(hours, minutes, seconds);
                    }
                    catch { error = true; }
                    break;
                case ConfigType.Username:
                    if (bot.GetUserID(value) < 10)
                        error = true;
                    else
                        objValue = Format.UppercaseFirst(value);
                    break;
            }
            if (error)
            {
                bot.SendReply(e, "Invalid Value: " + HTML.CreateColorString(bot.ColorHeaderHex, value));
                return;
            }
            if (bot.Configuration.Set(type, section, key, objValue))
                bot.SendReply(e, "Configuration entry " + HTML.CreateColorString(bot.ColorHeaderHex, section + "::" + key) + " has been set to: " + HTML.CreateColorString(bot.ColorHeaderHex, value));
            else
                bot.SendReply(e, "Unknown error while storing settings!");
        }

        private void OnConfigurationColorCommand(BotShell bot, CommandArgs e)
        {
            if (e.Args.Length < 2)
            {
                bot.SendReply(e, "Usage: configuration color [plugin] [key]");
                return;
            }
            if (!bot.Configuration.IsRegistered(e.Args[0], e.Args[1]))
            {
                bot.SendReply(e, "No such configuration entry");
                return;
            }
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle();
            bool first = true;
            string section = e.Args[0].ToLower();
            string key = e.Args[1].ToLower();
            string command = "configuration set " + section + " " + key + " ";
            foreach (KeyValuePair<string, string> color in this.Colors)
            {
                if (color.Value != string.Empty)
                {
                    window.AppendString("  ");
                    window.AppendBotCommandStart(command + color.Value, true);
                    window.AppendColorString(color.Value, color.Key);
                    window.AppendCommandEnd();
                    window.AppendLineBreak();
                }
                else
                {
                    if (!first)
                        window.AppendLineBreak();
                    window.AppendHeader(color.Key);
                    first = false;
                }
            }
            window.AppendLineBreak();
            window.AppendHeader("Other");
            window.AppendHighlight("To select a different color not listed above use: ");
            window.AppendLineBreak();
            window.AppendNormal("/tell " + bot.Character + " " + command + "<color hex>");
            bot.SendReply(e, "Color Selection »» ", window);
        }

        private void OnConfigurationResetCommand(BotShell bot, CommandArgs e)
        {
            if (e.Args.Length < 2)
            {
                bot.SendReply(e, "Usage: configuration reset [plugin] [key]");
                return;
            }
            if (!bot.Configuration.IsRegistered(e.Args[0], e.Args[1]))
            {
                bot.SendReply(e, "No such configuration entry");
                return;
            }
            string section = e.Args[0].ToLower();
            string key = e.Args[1].ToLower();
            ConfigurationEntry entry = bot.Configuration.GetRegistered(section, key);
            bot.Configuration.Set(entry.Type, section, key, entry.DefaultValue);
            bot.Configuration.Delete(section, key);
            bot.SendReply(e, "Configuration entry " + HTML.CreateColorString(bot.ColorHeaderHex, section + "::" + key) + " has been reset to it's default value");
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {
                case "configuration":
                    return "Displays the central configuration interface.\nThis interface displays all settings that have been registered by plugins.\n" +
                        "Usage: /tell " + bot.Character + " configuration [[plugin]]";
                case "configuration set":
                    return "Allows you to set a configuration entry.\nIt's recommended to approach configuration entries using the central interface by issueing the 'configuration' command.\n" +
                        "Usage: /tell " + bot.Character + " configuration set [plugin] [key] [value]";
                case "configuration reset":
                    return "Allows you to reset a configuration entry to it's default value.\n" +
                        "Usage: /tell " + bot.Character + " configuration reset [plugin] [key]";
            }
            return null;
        }
    }
}
