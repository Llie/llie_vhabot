== VhaBot - Llie's Edition ==

This version of VhaBot was forked from version 0.7.9 of VhaBot.
Although the original VhaBot is no longer being actively developed or
supported, there are other on-going efforts to enhance the VhaBot core.

I focus my development almost entirely on adding functionality to
VhaBot through the plug-in system.  For this reason, the core of the
bot in this fork has only minor changes.

My versions are tagged with a "LE" in the tag label and my version
numbers have no correlation with any tagged versions of the original
version.  Due to the fact that there is a limited number of users of
my fork of VhaBot, I will continue to develop along the 0.7 version
number and remain "in beta".  This version has a lot of useful
features and plug-ins, but it is not rigorously tested, and should
always be considered "beta" software.

To follow the my latest developments, please see the separate
sub-repositories that I created for the original plug-ins:

https://bitbucket.org/Llie/vhabot_plugins_default

my original/ported plug-ins:

https://bitbucket.org/Llie/vhabot_plugins_llie

For more information on this bot, please read:

https://bitbucket.org/Llie/llie_vhabot/wiki/Home

which includes instructions on how to configure and start up your own
VhaBot.
